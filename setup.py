from os import name
from setuptools import setup

setup(
    name = 'mm_db_dump',
    packages = ['mm_db_dump'],
    version = '0.0.1',
    install_requires=['pysqlcipher3'],
    entry_points={'console_scripts': ['mm_db_dump=mm_db_dump.dump:main']}
)