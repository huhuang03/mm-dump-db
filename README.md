mm_db_dump 可以拉取并解密已经root的安卓手机上的微信数据库

# 安装使用
1. 需要先安装Sqlcipher.
安装sqlcipher在windows上比较麻烦，可能需要自己编译。

2. 安装pysqlcipher3
```
pip3 install pysqlcipher3
```

3. 安装
正项目目录下运行
```
pip3 install -e .
```

4. 运行
```
mm_db_dump
```