# -*- coding: utf-8 -*-
import shutil
import subprocess
import os
from xml.etree.ElementTree import parse
from pysqlcipher3 import dbapi2 as sqlite
from hashlib import md5
import sqlite3
import re

uin = ""
imei = ""

# 中兴手机
ZTEC = "ztec"
# 联想手机
LeP = "lePhone"


# 创建临时文件
def createTmpDir():
    print("-- create tmp dir")
    if os.path.exists("tmp"):
        shutil.rmtree("tmp")

    os.makedirs("tmp")


# 提取sp文件
def getSqlFiles():
    print("-- getSqlFiles")
    os.system("adb shell rm -rf /sdcard/shared_prefs")
    os.system("adb shell 'su -c cp -r /data/data/com.tencent.mm/shared_prefs /sdcard/'")
    os.system("adb pull /sdcard/shared_prefs tmp/")

    os.system("adb shell rm -rf /sdcard/CompatibleInfo.cfg")
    os.system("adb shell 'su -c cp -r /data/data/com.tencent.mm/MicroMsg/CompatibleInfo.cfg /sdcard/'")
    os.system("adb pull /sdcard/CompatibleInfo.cfg tmp/CompatibleInfo.cfg")

# 获取uin，获取imei
def getUinImei():
    print("-- getUinImei")
    global uin
    global imei
    tree = parse("tmp/shared_prefs/system_config_prefs.xml")
    root = tree.getroot()
    for child in root:
        if child.attrib["name"] == "default_uin":
            uin = child.attrib["value"]
    if uin is None:
        print("uin is null")
    else:
        print("uin: " + uin)

    imei = getImeiFromWechatCache()
    if imei == "":
        if getPhoneType() == ZTEC:
            imei = subprocess.getstatusoutput("adb shell 'su -c service call iphonesubinfo 1' | awk -F \"'\" '{print $2}' | sed ' 1 d'")[1].replace(".", "").replace(" ", "").replace("\n", "")
        else:
            imei = subprocess.getstatusoutput("adb shell 'su -c service call iphonesubinfo 1' | awk -F \"'\" '{print $2}' | sed '1 d'| tr -d '\n' | tr -d '.' | tr -d ' '")[1]
    print("imei: " + imei)

def getImeiFromWechatCache():
    """如果微信缓存了imei。就从缓存中读取"""
    if not os.path.isfile("tmp/CompatibleInfo.cfg"):
        print("没有拿到CompatibleInfo文件，系统将退出")
        exit(-1)
    content = open("tmp/Compatibleinfo.cfg", "r", errors='ignore', encoding='utf-8').read()
    regex = r"\D(\d{15})\D"

    s = re.search(regex, content)
    if not s:
        regex = r"\D(\d{14})\D"
        s = re.search(regex, content)

    if s:
        print("发现缓存imei")
        return s.group(1)

    print("没有发现缓存imei")
    return ""


def getMd5(origin):
    print("-- getMd5")
    return md5(origin.encode('utf-8')).hexdigest()


def getPhoneType():
    """ 得到手机类型，目前适配两种手机，中兴和联想 """
    devices = subprocess.getstatusoutput("adb devices")[1]
    if "ZTEC" in devices:
        return ZTEC
    else:
        return LeP


fileName = ""

# 算出文件路径
def getFilePath():
    print("-- getFilePath")
    global fileName
    fileName = getMd5("mm" + uin)
    print("fileName: " + fileName)

# 提取db文件
def getDbFiles():
    print("-- getDbFiles")
    getOtherFile('EnMicroMsg.db')
    getOtherFile("EnMicroMsg.db-shm")
    getOtherFile("EnMicroMsg.db-wal")
    getOtherFile("EnMicroMsg.db-ini")

password = ""
def getPassword():
    print("-- getPassword")
    global password
    password = getMd5(imei + uin)[:7]
    print("password: " + password)

# 解密数据库
def decodeDb():
    print("-- decodeDb")
    conn = sqlite.connect("tmp" + "/EnMicroMsg.db")
    c = conn.cursor()
    c.execute("PRAGMA key = '" + password + "';")
    c.execute("PRAGMA cipher_use_hmac = OFF;")
    c.execute("PRAGMA cipher_page_size = 1024;")
    c.execute("PRAGMA kdf_iter = 4000;")
    c.execute("ATTACH DATABASE 'tmp/decrypted" + ".db' AS db KEY '';")
    c.execute("SELECT sqlcipher_export('db');" )
    c.execute("DETACH DATABASE db;" )
    c.close()


weixinNum = ""
# 提取微信号
def getWeixinNum():
    global weixinNum
    dbFile = "tmp/decrypted.db"
    print("file exists: " + str(os.path.exists(dbFile)))
    conn = sqlite3.connect(dbFile)
    cursor = conn.execute("select * from userinfo where id = 42;")
    for row in cursor:
        weixinNum = row[2]
    print("weixinNum: " + weixinNum)
    cursor.close()

    if weixinNum == "":
        cursor = conn.execute("select * from userinfo where id = 2;")
        for row in cursor:
            weixinNum = row[2]
        print("weixinNum: " + weixinNum)
        cursor.close()

def getOtherFile(fn):
    os.system("adb shell rm -rf /sdcard/%s" % fn)
    os.system("adb shell 'su -c cp -r /data/data/com.tencent.mm/MicroMsg/%s/%s /sdcard/'" % (fileName, fn))
    os.system("adb pull /sdcard/%s tmp/" % fn)

# 修改db文件名称
def changeDbName():
    os.system("mv tmp/decrypted.db tmp/" + weixinNum + ".db")


# 创建新文件夹，并移到新文件夹
def createNewFile():
    if not os.path.exists("dist/"):
        os.mkdir("dist/")

    dist_path = "dist/" + weixinNum + "/"
    if os.path.exists(dist_path):
        shutil.rmtree(dist_path)
    os.system("mv tmp " + dist_path)


def openFile():
    os.system("open " + "dist/" + weixinNum)


if __name__ == "__main__":
    createTmpDir()
    getSqlFiles()
    getUinImei()
    getFilePath()
    getDbFiles()
    getPassword()
    decodeDb()
    getWeixinNum()
    changeDbName()
    # 提取朋友圈数据文件
    getOtherFile("SnsMicroMsg.db")
    getOtherFile("FTS5IndexMicroMsg.db")
    getOtherFile("FTS5IndexMicroMsg.db-journal")
    createNewFile()
    openFile()
