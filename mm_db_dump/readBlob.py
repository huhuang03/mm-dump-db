"""
Yes, the blob is google proto.

But I don't know at that time.

So I wrote this parser manually..
"""
#!/usr/local/bin/python3
import sqlite3
import io

f = open("/Users/th/source/wechat_crack/test.b", "rb")


# def writeByteFile():
#     db = sqlite3.connect(
#         "/Users/yi/source/wechat_crack/yanhuo_0521/yanhuo_0521.db")
#     cu = db.cursor()
#     cu.execute("select roomdata from chatroom "
#                + "where chatroomname = '8056461973@chatroom'")
#     chatroom = cu.fetchone()[0]
#     print(chatroom)
#     print(type(chatroom))

#     f = open("test.b", "w")
#     f.write(chatroom)


def nextByte(f):
    b = f.read(1)
    if len(b) > 0:
        return b[0]
    else:
        return 0


def getInt(f):
    return nextByte(f)


def getType(f):
    rst = getTypeByInt(getInt(f))
    print("type: {}".format(rst))
    return rst


def getTypeByInt(i):
    return i >> 3


def getString(f):
    size = getInt(f)
    return f.read(size).decode("utf-8")


def splitBytes(f):
    rst = []
    size = getInt(f)
    rst.append(f.read(size))
    i = getInt(f)
    while getTypeByInt(i) == 1:
        size = getInt(f)
        rst.append(f.read(size))
        i = getInt(f)
    f.seek(-1, 1)
    return rst


def parseSingleRoomData(bs):
    f = io.BytesIO(bs)
    tag = getType(f)
    while tag != 0:
        if tag == 1:
            print("usename: " + getString(f))
        elif tag == 2:
            print("nickname: " + getString(f))
        elif tag == 3:
            print("css: {}".format(getInt(f)))
        elif tag == 4:
            print("cct: " + getString(f))
        tag = getType(f)


if __name__ == "__main__":
    if getType(f) == 1:
        bs = splitBytes(f)
        for b in bs:
            parseSingleRoomData(b)
