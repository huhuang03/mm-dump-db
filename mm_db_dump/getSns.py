# -*- coding: utf-8 -*-
import shutil
import commands
import os
from xml.etree.ElementTree import parse
from pysqlcipher import dbapi2 as sqlite
from hashlib import md5
import sqlite3
import re

uin = ""
imei = ""

# 中兴手机
ZTEC = "ztec"
# 联想手机
LeP = "lePhone"


# 创建临时文件
def createTmpDir():
    print("-- create tmp dir")
    if os.path.exists("tmp"):
        shutil.rmtree("tmp")

    os.makedirs("tmp")


# 提取sp文件
def getSqlFiles():
    print("-- getSqlFiles")
    os.system("adb shell rm -rf /sdcard/shared_prefs")
    os.system("adb shell 'su -c cp -r /data/data/com.tencent.mm/shared_prefs /sdcard/'")
    os.system("adb pull /sdcard/shared_prefs tmp/")

    os.system("adb shell rm -rf /sdcard/CompatibleInfo.cfg")
    os.system("adb shell 'su -c cp -r /data/data/com.tencent.mm/MicroMsg/CompatibleInfo.cfg /sdcard/'")
    os.system("adb pull /sdcard/CompatibleInfo.cfg tmp/CompatibleInfo.cfg")

# 获取uin，获取imei
def getUinImei():
    print("-- getUinImei")
    global uin
    global imei
    tree = parse("tmp/shared_prefs/system_config_prefs.xml")
    root = tree.getroot()
    for child in root:
        if child.attrib["name"] == "default_uin":
            uin = child.attrib["value"]
    if uin is None:
        print("uin is null")
    else:
        print("uin: " + uin)

    imei = getImeiFromWechatCache()
    if imei == "":
        if getPhoneType() == ZTEC:
            imei = commands.getstatusoutput("adb shell 'su -c service call iphonesubinfo 1' | awk -F \"'\" '{print $2}' | sed ' 1 d'")[1].replace(".", "").replace(" ", "").replace("\n", "")
        else:
            imei = commands.getstatusoutput("adb shell 'su -c service call iphonesubinfo 1' | awk -F \"'\" '{print $2}' | sed '1 d'| tr -d '\n' | tr -d '.' | tr -d ' '")[1]
    print("imei: " + imei)

def getImeiFromWechatCache():
    """如果微信缓存了imei。就从缓存中读取"""
    if not os.path.isfile("tmp/CompatibleInfo.cfg"):
        print("没有拿到CompatibleInfo文件，系统将退出")
        exit(-1)
    content = open("tmp/Compatibleinfo.cfg", "r").read()
    print content
    regex = r"\D(\d{15})\D"

    s = re.search(regex, content)
    if s:
        print("发现缓存imei")
        return s.group(1)

    print("没有发现缓存imei")
    return ""


def getMd5(origin):
    print("-- getMd5")
    return md5(origin).hexdigest()


def getPhoneType():
    """ 得到手机类型，目前适配两种手机，中兴和联想 """
    devices = commands.getstatusoutput("adb devices")[1]
    if "ZTEC" in devices:
        return ZTEC
    else:
        return LeP


fileName = ""

# 算出文件路径
def getFilePath():
    print("-- getFilePath")
    global fileName
    fileName = getMd5("mm" + uin)
    print("fileName: " + fileName)




def getOtherFile(fn):
    os.system("adb shell rm -rf /sdcard/%s" % fn)
    os.system("adb shell 'su -c cp -r /data/data/com.tencent.mm/MicroMsg/%s/%s /sdcard/'" % (fileName, fn))
    os.system("adb pull /sdcard/%s tmp/" % fn)




def openFile():
    os.system("open tmp")


if __name__ == "__main__":
    createTmpDir()
    getSqlFiles()
    getUinImei()
    getFilePath()
    # 提取朋友圈数据文件
    getOtherFile("SnsMicroMsg.db")
    getOtherFile("FTS5IndexMicroMsg.db")
    getOtherFile("FTS5IndexMicroMsg.db-journal")
    openFile()
